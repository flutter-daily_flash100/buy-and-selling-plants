import 'package:flutter/material.dart';
import 'package:flutter_application_1/secondpg.dart';

class first extends StatefulWidget {
  const first({super.key});

  State<first> createState() => firststate();
}

class firststate extends State<first> {
  Widget build(BuildContext context) {
    Color color1 = Color.fromRGBO(62, 102, 24, 1);
    Color color2 = Color.fromRGBO(124, 180, 70, 1);
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 251, 246, 246),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: Column(
        children: [
          Image.asset(
            "assets/first.png",
          ),
          Padding(
            padding: const EdgeInsets.only(right: 30),
            child: Container(
              child: Text(
                "Enjoy your",
                style: TextStyle(fontSize: 34, fontWeight: FontWeight.w400),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 115),
            child: Row(
              children: [
                Container(
                  child: Text(
                    "life with",
                    style: TextStyle(fontSize: 34, fontWeight: FontWeight.w400),
                  ),
                ),
                SizedBox(
                  width: 2,
                ),
                Row(
                  children: [
                    Container(
                      child: Text(
                        "plants",
                        style: TextStyle(
                            fontSize: 34, fontWeight: FontWeight.w600),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 80,
          ),
          InkWell(
              child: Container(
                height: 50,
                width: 320,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: -3,
                          blurRadius: 5,
                          offset: Offset(-1, 2)),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [color2, color1])),
                child: Padding(
                  padding: const EdgeInsets.only(left: 120, top: 12),
                  child: Container(
                    child: Text(
                      "Get Started >",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => second(),
                    ));
              })
        ],
      ),
    );
  }
}
