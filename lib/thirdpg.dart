import 'package:flutter/material.dart';
import 'package:flutter_application_1/fourth.dart';

class third extends StatefulWidget {
  const third({super.key});

  State<third> createState() => thirdstate();
}

class thirdstate extends State<third> {
  Color color1 = Color.fromRGBO(62, 102, 24, 1);
  Color color2 = Color.fromRGBO(124, 180, 70, 1);
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 251, 246, 246),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Verification",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
            ),
            Container(
              child: Text(
                "Enter the OTP code from the phone we",
                style: TextStyle(color: const Color.fromRGBO(0, 0, 0, 0.6)),
              ),
            ),
            Container(
              child: Text(
                "just sent you",
                style: TextStyle(color: const Color.fromRGBO(0, 0, 0, 0.6)),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Color.fromARGB(255, 116, 114, 114))),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Color.fromARGB(255, 116, 114, 114))),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Color.fromARGB(255, 116, 114, 114))),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Color.fromARGB(255, 116, 114, 114))),
                ),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 18),
              child: Row(
                children: [
                  Container(
                    child: Text(
                      "Don’t receive OTP code!",
                      style:
                          TextStyle(color: const Color.fromRGBO(0, 0, 0, 0.6)),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Container(
                    child: Text(
                      "Resend",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),
            InkWell(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                      height: 50,
                      width: 380,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: -1,
                                blurRadius: 5,
                                offset: Offset(-1, 3)),
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [color2, color1])),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 133, top: 12),
                        child: Container(
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      )),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => four(),
                      ));
                })
          ],
        ),
      ),
    );
  }
}
