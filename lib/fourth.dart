import 'package:flutter/material.dart';
import 'package:flutter_application_1/fifth.dart';
import 'package:flutter_application_1/thirdpg.dart';

class four extends StatefulWidget {
  const four({super.key});

  State<four> createState() => fourstate();
}

class fourstate extends State<four> {
  Color color1 = Color.fromRGBO(62, 102, 24, 1);
  Color color2 = Color.fromRGBO(124, 180, 70, 1);
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 251, 246, 246),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(right: 47),
                        child: Text(
                          "Find your",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 24),
                        )),
                    Container(
                        child: Text(
                      "favorite plant",
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
                    )),
                  ],
                ),
                SizedBox(
                  width: 150,
                ),
                Container(
                  height: 40,
                  width: 40,
                  child: Icon(Icons.shopping_bag_outlined),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Container(
                      height: 108,
                      width: 310,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color.fromRGBO(204, 231, 185, 1)),
                      child: Row(
                        children: [
                          Column(
                            children: [
                              Container(
                                  padding: EdgeInsets.only(left: 40, top: 20),
                                  child: Text(
                                    "30% OFF",
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600),
                                  )),
                              Container(
                                padding: EdgeInsets.only(
                                  left: 40,
                                ),
                                child: Text(
                                  "02-23 April",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(0, 0, 0, 0.6)),
                                ),
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 40),
                            child: Image.asset("assets/third.png"),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 108,
                      width: 310,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color.fromRGBO(204, 231, 185, 1)),
                      child: Row(
                        children: [
                          Column(
                            children: [
                              Container(
                                  padding: EdgeInsets.only(left: 40, top: 20),
                                  child: Text(
                                    "30% OFF",
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600),
                                  )),
                              Container(
                                padding: EdgeInsets.only(
                                  left: 40,
                                ),
                                child: Text(
                                  "02-23 April",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(0, 0, 0, 0.6)),
                                ),
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 40),
                            child: Image.asset("assets/third.png"),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 108,
                      width: 310,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color.fromRGBO(204, 231, 185, 1)),
                      child: Row(
                        children: [
                          Column(
                            children: [
                              Container(
                                  padding: EdgeInsets.only(left: 40, top: 20),
                                  child: Text(
                                    "30% OFF",
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600),
                                  )),
                              Container(
                                padding: EdgeInsets.only(
                                  left: 40,
                                ),
                                child: Text(
                                  "02-23 April",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(0, 0, 0, 0.6)),
                                ),
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 40),
                            child: Image.asset("assets/third.png"),
                          )
                        ],
                      ),
                    ),
                  ],
                )),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.only(right: 280),
              child: Text(
                "Indoor",
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  InkWell(
                      child: Container(
                        height: 188,
                        width: 141,
                        color: Colors.white,
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.only(left: 26),
                                  child: Image.asset("assets/fifth.png"),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 20, right: 20),
                                  child: Text(
                                    "Snake Plants",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromRGBO(48, 48, 48, 1)),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(right: 50),
                                  child: Text(
                                    "₹350",
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromRGBO(62, 102, 24, 1)),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => five(),
                            ));
                      }),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 188,
                    width: 141,
                    color: Colors.white,
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 26),
                              child: Image.asset("assets/fifth.png"),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20, right: 20),
                              child: Text(
                                "Snake Plants",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(48, 48, 48, 1)),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 50),
                              child: Text(
                                "₹350",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(62, 102, 24, 1)),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 188,
                    width: 141,
                    color: Colors.white,
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 26),
                              child: Image.asset("assets/fifth.png"),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20, right: 20),
                              child: Text(
                                "Snake Plants",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(48, 48, 48, 1)),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 50),
                              child: Text(
                                "₹350",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(62, 102, 24, 1)),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.only(right: 275),
              child: Text(
                "outdoor",
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    height: 188,
                    width: 141,
                    color: Colors.white,
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 26),
                              child: Image.asset("assets/fifth.png"),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20, right: 20),
                              child: Text(
                                "Snake Plants",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(48, 48, 48, 1)),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 50),
                              child: Text(
                                "₹350",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(62, 102, 24, 1)),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 188,
                    width: 141,
                    color: Colors.white,
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 26),
                              child: Image.asset("assets/fifth.png"),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20, right: 20),
                              child: Text(
                                "Snake Plants",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(48, 48, 48, 1)),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 50),
                              child: Text(
                                "₹350",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(62, 102, 24, 1)),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 188,
                    width: 141,
                    color: Colors.white,
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 26),
                              child: Image.asset("assets/fifth.png"),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20, right: 20),
                              child: Text(
                                "Snake Plants",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(48, 48, 48, 1)),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 50),
                              child: Text(
                                "₹350",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(62, 102, 24, 1)),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
//