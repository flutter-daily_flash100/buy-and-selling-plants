import 'package:flutter/material.dart';
import 'package:flutter_application_1/fourth.dart';

class five extends StatefulWidget {
  const five({super.key});

  State<five> createState() => fivestate();
}

class fivestate extends State<five> {
  Color color1 = Color.fromRGBO(62, 102, 24, 1);
  Color color2 = Color.fromRGBO(124, 180, 70, 1);
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 60),
              child: Container(
                child: Image.asset("assets/six.png"),
              ),
            ),
            SizedBox(
              height: 35,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 50),
              child: Container(
                child: Text(
                  "Snake Plants",
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(48, 48, 48, 1)),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 55),
              child: Container(
                child: Text(
                  "Plansts make your life with minimal and",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(48, 48, 48, 1)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 60),
              child: Container(
                child: Text(
                  "happy love the plants more and enjoy life.",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(48, 48, 48, 1)),
                ),
              ),
            ),
            SizedBox(
              height: 80,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 38),
              child: Container(
                height: 200,
                width: 320,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(118, 152, 75, 1),
                    borderRadius: BorderRadius.circular(15)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          child: Image.asset("assets/1.png"),
                        ),
                        SizedBox(
                          width: 40,
                        ),
                        Container(
                          child: Image.asset("assets/2.png"),
                        ),
                        SizedBox(
                          width: 40,
                        ),
                        Container(
                          child: Image.asset("assets/3.png"),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                child: Text(
                                  "Total prise",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color:
                                          Color.fromRGBO(255, 255, 255, 0.8)),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "₹ 350",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color:
                                          Color.fromRGBO(255, 255, 255, 0.8)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 90,
                        ),
                        Container(
                          child: Image.asset("assets/button.png"),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
