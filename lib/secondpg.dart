import 'package:flutter/material.dart';
import 'package:flutter_application_1/thirdpg.dart';

class second extends StatefulWidget {
  const second({super.key});

  State<second> createState() => secondstate();
}

class secondstate extends State<second> {
  Color color1 = Color.fromRGBO(62, 102, 24, 1);
  Color color2 = Color.fromRGBO(124, 180, 70, 1);
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 251, 246, 246),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          children: [
            Container(
              child: Text(
                "Log in",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              width: 320,
              height: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: -3, blurRadius: 5, offset: Offset(-1, 2)),
                  ],
                  color: Colors.white,
                  border:
                      Border.all(color: Color.fromARGB(255, 116, 114, 114))),
              child: Container(
                  child: Row(
                children: [
                  SizedBox(
                    width: 15,
                  ),
                  Icon(
                    Icons.call,
                    color: Color.fromARGB(255, 149, 144, 144),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    "Mobile Number",
                    style: TextStyle(
                      fontSize: 13,
                      color: Color.fromARGB(255, 149, 144, 144),
                    ),
                  )
                ],
              )),
            ),
            SizedBox(
              height: 40,
            ),
            InkWell(
                child: Container(
                    height: 50,
                    width: 320,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              spreadRadius: -1,
                              blurRadius: 5,
                              offset: Offset(-1, 3)),
                        ],
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [color2, color1])),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 133, top: 12),
                      child: Container(
                        child: Text(
                          "Log in",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    )),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => third(),
                      ));
                }),
            SizedBox(
              height: 80,
            ),
            Container(
              child: Image.asset(
                "assets/second2.png",
              ),
            )
          ],
        ),
      ),
    );
  }
}
