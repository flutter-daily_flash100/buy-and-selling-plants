import 'package:flutter/material.dart';
import 'package:flutter_application_1/fifth.dart';
import 'package:flutter_application_1/firstpg.dart';
import 'package:flutter_application_1/fourth.dart';
import 'package:flutter_application_1/secondpg.dart';
import 'package:flutter_application_1/thirdpg.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: first(),
      ),
    );
  }
}
